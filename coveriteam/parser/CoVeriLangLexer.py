# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# Generated from CoVeriLang.g4 by ANTLR 4.8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\63")
        buf.write("\u01ff\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write('\t\36\4\37\t\37\4 \t \4!\t!\4"\t"\4#\t#\4$\t$\4%\t%')
        buf.write("\4&\t&\4'\t'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\3\2\3\2\3\2\3")
        buf.write("\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t")
        buf.write("\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3")
        buf.write("\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f")
        buf.write("\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3")
        buf.write("\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3")
        buf.write("\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20")
        buf.write("\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\22")
        buf.write("\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23")
        buf.write("\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23")
        buf.write("\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24")
        buf.write("\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27")
        buf.write("\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\31\3\31")
        buf.write("\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31")
        buf.write("\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32")
        buf.write("\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\33")
        buf.write("\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33")
        buf.write("\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33")
        buf.write("\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33")
        buf.write("\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\35\3\35")
        buf.write("\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35")
        buf.write("\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35")
        buf.write("\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3")
        buf.write(' \3!\3!\3!\3!\3"\3"\3"\3"\3"\3"\3"\3"\3"\3"')
        buf.write('\3"\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3$\3')
        buf.write("$\3$\3%\3%\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\3")
        buf.write("&\3&\3&\3&\3&\3&\3&\3&\5&\u01b4\n&\3'\3'\3'\3'\3'")
        buf.write("\3'\3'\3'\3'\5'\u01bf\n'\3(\3(\3(\3(\7(\u01c5\n")
        buf.write("(\f(\16(\u01c8\13(\3)\3)\7)\u01cc\n)\f)\16)\u01cf\13)")
        buf.write("\3)\3)\3*\3*\3*\7*\u01d6\n*\f*\16*\u01d9\13*\3+\3+\5+")
        buf.write("\u01dd\n+\3,\3,\3-\3-\3.\3.\3/\5/\u01e6\n/\3/\3/\3/\3")
        buf.write("/\3\60\6\60\u01ed\n\60\r\60\16\60\u01ee\3\60\3\60\3\61")
        buf.write("\3\61\3\62\3\62\3\62\3\62\7\62\u01f9\n\62\f\62\16\62\u01fc")
        buf.write("\13\62\3\62\3\62\2\2\63\3\3\5\4\7\5\t\6\13\7\r\b\17\t")
        buf.write("\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23")
        buf.write("%\24'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36")
        buf.write(";\37= ?!A\"C#E$G%I&K'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63")
        buf.write('\3\2\t\3\2aa\3\2$$\3\2c|\3\2C\\\3\2\62;\4\2\13\13""')
        buf.write("\4\2\f\f\17\17\2\u020d\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2")
        buf.write("\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2")
        buf.write("\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2")
        buf.write("\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!")
        buf.write("\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2'\3\2\2\2\2)\3\2\2\2")
        buf.write("\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3")
        buf.write("\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2")
        buf.write("\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2")
        buf.write("\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2")
        buf.write("\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3")
        buf.write("\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c")
        buf.write("\3\2\2\2\3e\3\2\2\2\5i\3\2\2\2\7k\3\2\2\2\tm\3\2\2\2\13")
        buf.write("o\3\2\2\2\rq\3\2\2\2\17s\3\2\2\2\21u\3\2\2\2\23|\3\2\2")
        buf.write("\2\25\u0085\3\2\2\2\27\u008c\3\2\2\2\31\u009c\3\2\2\2")
        buf.write("\33\u009e\3\2\2\2\35\u00b3\3\2\2\2\37\u00bc\3\2\2\2!\u00c0")
        buf.write("\3\2\2\2#\u00c7\3\2\2\2%\u00d0\3\2\2\2'\u00e3\3\2\2\2")
        buf.write(")\u00ea\3\2\2\2+\u00f1\3\2\2\2-\u00fc\3\2\2\2/\u0101\3")
        buf.write("\2\2\2\61\u0108\3\2\2\2\63\u0119\3\2\2\2\65\u012a\3\2")
        buf.write("\2\2\67\u014c\3\2\2\29\u0155\3\2\2\2;\u016d\3\2\2\2=\u016f")
        buf.write("\3\2\2\2?\u0175\3\2\2\2A\u017a\3\2\2\2C\u017e\3\2\2\2")
        buf.write("E\u0189\3\2\2\2G\u0193\3\2\2\2I\u019b\3\2\2\2K\u01b3\3")
        buf.write("\2\2\2M\u01be\3\2\2\2O\u01c0\3\2\2\2Q\u01c9\3\2\2\2S\u01d2")
        buf.write("\3\2\2\2U\u01dc\3\2\2\2W\u01de\3\2\2\2Y\u01e0\3\2\2\2")
        buf.write("[\u01e2\3\2\2\2]\u01e5\3\2\2\2_\u01ec\3\2\2\2a\u01f2\3")
        buf.write("\2\2\2c\u01f4\3\2\2\2ef\7h\2\2fg\7w\2\2gh\7p\2\2h\4\3")
        buf.write("\2\2\2ij\7*\2\2j\6\3\2\2\2kl\7+\2\2l\b\3\2\2\2mn\7}\2")
        buf.write("\2n\n\3\2\2\2op\7\177\2\2p\f\3\2\2\2qr\7.\2\2r\16\3\2")
        buf.write("\2\2st\7?\2\2t\20\3\2\2\2uv\7r\2\2vw\7t\2\2wx\7k\2\2x")
        buf.write("y\7p\2\2yz\7v\2\2z{\7*\2\2{\22\3\2\2\2|}\7g\2\2}~\7z\2")
        buf.write("\2~\177\7g\2\2\177\u0080\7e\2\2\u0080\u0081\7w\2\2\u0081")
        buf.write("\u0082\7v\2\2\u0082\u0083\7g\2\2\u0083\u0084\7*\2\2\u0084")
        buf.write("\24\3\2\2\2\u0085\u0086\7t\2\2\u0086\u0087\7g\2\2\u0087")
        buf.write("\u0088\7v\2\2\u0088\u0089\7w\2\2\u0089\u008a\7t\2\2\u008a")
        buf.write("\u008b\7p\2\2\u008b\26\3\2\2\2\u008c\u008d\7u\2\2\u008d")
        buf.write("\u008e\7g\2\2\u008e\u008f\7v\2\2\u008f\u0090\7a\2\2\u0090")
        buf.write("\u0091\7c\2\2\u0091\u0092\7e\2\2\u0092\u0093\7v\2\2\u0093")
        buf.write("\u0094\7q\2\2\u0094\u0095\7t\2\2\u0095\u0096\7a\2\2\u0096")
        buf.write("\u0097\7p\2\2\u0097\u0098\7c\2\2\u0098\u0099\7o\2\2\u0099")
        buf.write("\u009a\7g\2\2\u009a\u009b\7*\2\2\u009b\30\3\2\2\2\u009c")
        buf.write("\u009d\7<\2\2\u009d\32\3\2\2\2\u009e\u009f\7C\2\2\u009f")
        buf.write("\u00a0\7e\2\2\u00a0\u00a1\7v\2\2\u00a1\u00a2\7q\2\2\u00a2")
        buf.write("\u00a3\7t\2\2\u00a3\u00a4\7H\2\2\u00a4\u00a5\7c\2\2\u00a5")
        buf.write("\u00a6\7e\2\2\u00a6\u00a7\7v\2\2\u00a7\u00a8\7q\2\2\u00a8")
        buf.write("\u00a9\7t\2\2\u00a9\u00aa\7{\2\2\u00aa\u00ab\7\60\2\2")
        buf.write("\u00ab\u00ac\7e\2\2\u00ac\u00ad\7t\2\2\u00ad\u00ae\7g")
        buf.write("\2\2\u00ae\u00af\7c\2\2\u00af\u00b0\7v\2\2\u00b0\u00b1")
        buf.write("\7g\2\2\u00b1\u00b2\7*\2\2\u00b2\34\3\2\2\2\u00b3\u00b4")
        buf.write("\7U\2\2\u00b4\u00b5\7G\2\2\u00b5\u00b6\7S\2\2\u00b6\u00b7")
        buf.write("\7W\2\2\u00b7\u00b8\7G\2\2\u00b8\u00b9\7P\2\2\u00b9\u00ba")
        buf.write("\7E\2\2\u00ba\u00bb\7G\2\2\u00bb\36\3\2\2\2\u00bc\u00bd")
        buf.write("\7K\2\2\u00bd\u00be\7V\2\2\u00be\u00bf\7G\2\2\u00bf \3")
        buf.write("\2\2\2\u00c0\u00c1\7T\2\2\u00c1\u00c2\7G\2\2\u00c2\u00c3")
        buf.write("\7R\2\2\u00c3\u00c4\7G\2\2\u00c4\u00c5\7C\2\2\u00c5\u00c6")
        buf.write('\7V\2\2\u00c6"\3\2\2\2\u00c7\u00c8\7R\2\2\u00c8\u00c9')
        buf.write("\7C\2\2\u00c9\u00ca\7T\2\2\u00ca\u00cb\7C\2\2\u00cb\u00cc")
        buf.write("\7N\2\2\u00cc\u00cd\7N\2\2\u00cd\u00ce\7G\2\2\u00ce\u00cf")
        buf.write("\7N\2\2\u00cf$\3\2\2\2\u00d0\u00d1\7R\2\2\u00d1\u00d2")
        buf.write("\7C\2\2\u00d2\u00d3\7T\2\2\u00d3\u00d4\7C\2\2\u00d4\u00d5")
        buf.write("\7N\2\2\u00d5\u00d6\7N\2\2\u00d6\u00d7\7G\2\2\u00d7\u00d8")
        buf.write("\7N\2\2\u00d8\u00d9\7a\2\2\u00d9\u00da\7R\2\2\u00da\u00db")
        buf.write("\7Q\2\2\u00db\u00dc\7T\2\2\u00dc\u00dd\7V\2\2\u00dd\u00de")
        buf.write("\7H\2\2\u00de\u00df\7Q\2\2\u00df\u00e0\7N\2\2\u00e0\u00e1")
        buf.write("\7K\2\2\u00e1\u00e2\7Q\2\2\u00e2&\3\2\2\2\u00e3\u00e4")
        buf.write("\7L\2\2\u00e4\u00e5\7q\2\2\u00e5\u00e6\7k\2\2\u00e6\u00e7")
        buf.write("\7p\2\2\u00e7\u00e8\7g\2\2\u00e8\u00e9\7t\2\2\u00e9(\3")
        buf.write("\2\2\2\u00ea\u00eb\7U\2\2\u00eb\u00ec\7g\2\2\u00ec\u00ed")
        buf.write("\7v\2\2\u00ed\u00ee\7v\2\2\u00ee\u00ef\7g\2\2\u00ef\u00f0")
        buf.write("\7t\2\2\u00f0*\3\2\2\2\u00f1\u00f2\7E\2\2\u00f2\u00f3")
        buf.write("\7q\2\2\u00f3\u00f4\7o\2\2\u00f4\u00f5\7r\2\2\u00f5\u00f6")
        buf.write("\7c\2\2\u00f6\u00f7\7t\2\2\u00f7\u00f8\7c\2\2\u00f8\u00f9")
        buf.write("\7v\2\2\u00f9\u00fa\7q\2\2\u00fa\u00fb\7t\2\2\u00fb,\3")
        buf.write("\2\2\2\u00fc\u00fd\7E\2\2\u00fd\u00fe\7q\2\2\u00fe\u00ff")
        buf.write("\7r\2\2\u00ff\u0100\7{\2\2\u0100.\3\2\2\2\u0101\u0102")
        buf.write("\7T\2\2\u0102\u0103\7g\2\2\u0103\u0104\7p\2\2\u0104\u0105")
        buf.write("\7c\2\2\u0105\u0106\7o\2\2\u0106\u0107\7g\2\2\u0107\60")
        buf.write("\3\2\2\2\u0108\u0109\7V\2\2\u0109\u010a\7g\2\2\u010a\u010b")
        buf.write("\7u\2\2\u010b\u010c\7v\2\2\u010c\u010d\7U\2\2\u010d\u010e")
        buf.write("\7r\2\2\u010e\u010f\7g\2\2\u010f\u0110\7e\2\2\u0110\u0111")
        buf.write("\7V\2\2\u0111\u0112\7q\2\2\u0112\u0113\7U\2\2\u0113\u0114")
        buf.write("\7r\2\2\u0114\u0115\7g\2\2\u0115\u0116\7e\2\2\u0116\u0117")
        buf.write("\7*\2\2\u0117\u0118\7+\2\2\u0118\62\3\2\2\2\u0119\u011a")
        buf.write("\7U\2\2\u011a\u011b\7r\2\2\u011b\u011c\7g\2\2\u011c\u011d")
        buf.write("\7e\2\2\u011d\u011e\7V\2\2\u011e\u011f\7q\2\2\u011f\u0120")
        buf.write("\7V\2\2\u0120\u0121\7g\2\2\u0121\u0122\7u\2\2\u0122\u0123")
        buf.write("\7v\2\2\u0123\u0124\7U\2\2\u0124\u0125\7r\2\2\u0125\u0126")
        buf.write("\7g\2\2\u0126\u0127\7e\2\2\u0127\u0128\7*\2\2\u0128\u0129")
        buf.write("\7+\2\2\u0129\64\3\2\2\2\u012a\u012b\7E\2\2\u012b\u012c")
        buf.write("\7n\2\2\u012c\u012d\7c\2\2\u012d\u012e\7u\2\2\u012e\u012f")
        buf.write("\7u\2\2\u012f\u0130\7k\2\2\u0130\u0131\7h\2\2\u0131\u0132")
        buf.write("\7k\2\2\u0132\u0133\7e\2\2\u0133\u0134\7c\2\2\u0134\u0135")
        buf.write("\7v\2\2\u0135\u0136\7k\2\2\u0136\u0137\7q\2\2\u0137\u0138")
        buf.write("\7p\2\2\u0138\u0139\7V\2\2\u0139\u013a\7q\2\2\u013a\u013b")
        buf.write("\7C\2\2\u013b\u013c\7e\2\2\u013c\u013d\7v\2\2\u013d\u013e")
        buf.write("\7q\2\2\u013e\u013f\7t\2\2\u013f\u0140\7F\2\2\u0140\u0141")
        buf.write("\7g\2\2\u0141\u0142\7h\2\2\u0142\u0143\7k\2\2\u0143\u0144")
        buf.write("\7p\2\2\u0144\u0145\7k\2\2\u0145\u0146\7v\2\2\u0146\u0147")
        buf.write("\7k\2\2\u0147\u0148\7q\2\2\u0148\u0149\7p\2\2\u0149\u014a")
        buf.write("\7*\2\2\u014a\u014b\7+\2\2\u014b\66\3\2\2\2\u014c\u014d")
        buf.write("\7K\2\2\u014d\u014e\7f\2\2\u014e\u014f\7g\2\2\u014f\u0150")
        buf.write("\7p\2\2\u0150\u0151\7v\2\2\u0151\u0152\7k\2\2\u0152\u0153")
        buf.write("\7v\2\2\u0153\u0154\7{\2\2\u01548\3\2\2\2\u0155\u0156")
        buf.write("\7C\2\2\u0156\u0157\7t\2\2\u0157\u0158\7v\2\2\u0158\u0159")
        buf.write("\7k\2\2\u0159\u015a\7h\2\2\u015a\u015b\7c\2\2\u015b\u015c")
        buf.write("\7e\2\2\u015c\u015d\7v\2\2\u015d\u015e\7H\2\2\u015e\u015f")
        buf.write("\7c\2\2\u015f\u0160\7e\2\2\u0160\u0161\7v\2\2\u0161\u0162")
        buf.write("\7q\2\2\u0162\u0163\7t\2\2\u0163\u0164\7{\2\2\u0164\u0165")
        buf.write("\7\60\2\2\u0165\u0166\7e\2\2\u0166\u0167\7t\2\2\u0167")
        buf.write("\u0168\7g\2\2\u0168\u0169\7c\2\2\u0169\u016a\7v\2\2\u016a")
        buf.write("\u016b\7g\2\2\u016b\u016c\7*\2\2\u016c:\3\2\2\2\u016d")
        buf.write("\u016e\7\60\2\2\u016e<\3\2\2\2\u016f\u0170\7K\2\2\u0170")
        buf.write("\u0171\7N\2\2\u0171\u0172\7R\2\2\u0172\u0173\7\65\2\2")
        buf.write("\u0173\u0174\7\64\2\2\u0174>\3\2\2\2\u0175\u0176\7N\2")
        buf.write("\2\u0176\u0177\7R\2\2\u0177\u0178\78\2\2\u0178\u0179\7")
        buf.write("\66\2\2\u0179@\3\2\2\2\u017a\u017b\7P\2\2\u017b\u017c")
        buf.write("\7Q\2\2\u017c\u017d\7V\2\2\u017dB\3\2\2\2\u017e\u017f")
        buf.write("\7K\2\2\u017f\u0180\7P\2\2\u0180\u0181\7U\2\2\u0181\u0182")
        buf.write("\7V\2\2\u0182\u0183\7C\2\2\u0183\u0184\7P\2\2\u0184\u0185")
        buf.write("\7E\2\2\u0185\u0186\7G\2\2\u0186\u0187\7Q\2\2\u0187\u0188")
        buf.write("\7H\2\2\u0188D\3\2\2\2\u0189\u018a\7G\2\2\u018a\u018b")
        buf.write("\7N\2\2\u018b\u018c\7G\2\2\u018c\u018d\7O\2\2\u018d\u018e")
        buf.write("\7G\2\2\u018e\u018f\7P\2\2\u018f\u0190\7V\2\2\u0190\u0191")
        buf.write("\7Q\2\2\u0191\u0192\7H\2\2\u0192F\3\2\2\2\u0193\u0194")
        buf.write("\7V\2\2\u0194\u0195\7Q\2\2\u0195\u0196\7F\2\2\u0196\u0197")
        buf.write("\7Q\2\2\u0197\u0198\7/\2\2\u0198\u0199\7/\2\2\u0199\u019a")
        buf.write("\7/\2\2\u019aH\3\2\2\2\u019b\u019c\7)\2\2\u019cJ\3\2\2")
        buf.write("\2\u019d\u019e\7H\2\2\u019e\u019f\7C\2\2\u019f\u01a0\7")
        buf.write("N\2\2\u01a0\u01a1\7U\2\2\u01a1\u01a2\7G\2\2\u01a2\u01a3")
        buf.write("\3\2\2\2\u01a3\u01b4\b&\2\2\u01a4\u01a5\7V\2\2\u01a5\u01a6")
        buf.write("\7T\2\2\u01a6\u01a7\7W\2\2\u01a7\u01a8\7G\2\2\u01a8\u01a9")
        buf.write("\3\2\2\2\u01a9\u01b4\b&\3\2\u01aa\u01ab\7W\2\2\u01ab\u01ac")
        buf.write("\7P\2\2\u01ac\u01ad\7M\2\2\u01ad\u01ae\7P\2\2\u01ae\u01af")
        buf.write("\7Q\2\2\u01af\u01b0\7Y\2\2\u01b0\u01b1\7P\2\2\u01b1\u01b2")
        buf.write("\3\2\2\2\u01b2\u01b4\b&\4\2\u01b3\u019d\3\2\2\2\u01b3")
        buf.write("\u01a4\3\2\2\2\u01b3\u01aa\3\2\2\2\u01b4L\3\2\2\2\u01b5")
        buf.write("\u01b6\7C\2\2\u01b6\u01b7\7P\2\2\u01b7\u01bf\7F\2\2\u01b8")
        buf.write("\u01b9\7Q\2\2\u01b9\u01bf\7T\2\2\u01ba\u01bb\7?\2\2\u01bb")
        buf.write("\u01bf\7?\2\2\u01bc\u01bd\7#\2\2\u01bd\u01bf\7?\2\2\u01be")
        buf.write("\u01b5\3\2\2\2\u01be\u01b8\3\2\2\2\u01be\u01ba\3\2\2\2")
        buf.write("\u01be\u01bc\3\2\2\2\u01bfN\3\2\2\2\u01c0\u01c6\5W,\2")
        buf.write("\u01c1\u01c5\5U+\2\u01c2\u01c5\5[.\2\u01c3\u01c5\t\2\2")
        buf.write("\2\u01c4\u01c1\3\2\2\2\u01c4\u01c2\3\2\2\2\u01c4\u01c3")
        buf.write("\3\2\2\2\u01c5\u01c8\3\2\2\2\u01c6\u01c4\3\2\2\2\u01c6")
        buf.write("\u01c7\3\2\2\2\u01c7P\3\2\2\2\u01c8\u01c6\3\2\2\2\u01c9")
        buf.write("\u01cd\7$\2\2\u01ca\u01cc\n\3\2\2\u01cb\u01ca\3\2\2\2")
        buf.write("\u01cc\u01cf\3\2\2\2\u01cd\u01cb\3\2\2\2\u01cd\u01ce\3")
        buf.write("\2\2\2\u01ce\u01d0\3\2\2\2\u01cf\u01cd\3\2\2\2\u01d0\u01d1")
        buf.write("\7$\2\2\u01d1R\3\2\2\2\u01d2\u01d7\5Y-\2\u01d3\u01d6\5")
        buf.write("U+\2\u01d4\u01d6\5[.\2\u01d5\u01d3\3\2\2\2\u01d5\u01d4")
        buf.write("\3\2\2\2\u01d6\u01d9\3\2\2\2\u01d7\u01d5\3\2\2\2\u01d7")
        buf.write("\u01d8\3\2\2\2\u01d8T\3\2\2\2\u01d9\u01d7\3\2\2\2\u01da")
        buf.write("\u01dd\5W,\2\u01db\u01dd\5Y-\2\u01dc\u01da\3\2\2\2\u01dc")
        buf.write("\u01db\3\2\2\2\u01ddV\3\2\2\2\u01de\u01df\t\4\2\2\u01df")
        buf.write("X\3\2\2\2\u01e0\u01e1\t\5\2\2\u01e1Z\3\2\2\2\u01e2\u01e3")
        buf.write("\t\6\2\2\u01e3\\\3\2\2\2\u01e4\u01e6\7\17\2\2\u01e5\u01e4")
        buf.write("\3\2\2\2\u01e5\u01e6\3\2\2\2\u01e6\u01e7\3\2\2\2\u01e7")
        buf.write("\u01e8\7\f\2\2\u01e8\u01e9\3\2\2\2\u01e9\u01ea\b/\5\2")
        buf.write("\u01ea^\3\2\2\2\u01eb\u01ed\t\7\2\2\u01ec\u01eb\3\2\2")
        buf.write("\2\u01ed\u01ee\3\2\2\2\u01ee\u01ec\3\2\2\2\u01ee\u01ef")
        buf.write("\3\2\2\2\u01ef\u01f0\3\2\2\2\u01f0\u01f1\b\60\5\2\u01f1")
        buf.write("`\3\2\2\2\u01f2\u01f3\7=\2\2\u01f3b\3\2\2\2\u01f4\u01f5")
        buf.write("\7\61\2\2\u01f5\u01f6\7\61\2\2\u01f6\u01fa\3\2\2\2\u01f7")
        buf.write("\u01f9\n\b\2\2\u01f8\u01f7\3\2\2\2\u01f9\u01fc\3\2\2\2")
        buf.write("\u01fa\u01f8\3\2\2\2\u01fa\u01fb\3\2\2\2\u01fb\u01fd\3")
        buf.write("\2\2\2\u01fc\u01fa\3\2\2\2\u01fd\u01fe\b\62\5\2\u01fe")
        buf.write("d\3\2\2\2\16\2\u01b3\u01be\u01c4\u01c6\u01cd\u01d5\u01d7")
        buf.write("\u01dc\u01e5\u01ee\u01fa\6\3&\2\3&\3\3&\4\b\2\2")
        return buf.getvalue()


class CoVeriLangLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [DFA(ds, i) for i, ds in enumerate(atn.decisionToState)]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    T__15 = 16
    T__16 = 17
    T__17 = 18
    T__18 = 19
    T__19 = 20
    T__20 = 21
    T__21 = 22
    T__22 = 23
    T__23 = 24
    T__24 = 25
    T__25 = 26
    T__26 = 27
    T__27 = 28
    T__28 = 29
    T__29 = 30
    T__30 = 31
    T__31 = 32
    T__32 = 33
    T__33 = 34
    T__34 = 35
    T__35 = 36
    VERDICT = 37
    BIN_OP = 38
    ID = 39
    STRING = 40
    TYPE_NAME = 41
    LETTER = 42
    LOWER_CASE = 43
    UPPER_CASE = 44
    DIGIT = 45
    NEWLINE = 46
    WS = 47
    DELIMITER = 48
    COMMENT = 49

    channelNames = ["DEFAULT_TOKEN_CHANNEL", "HIDDEN"]

    modeNames = ["DEFAULT_MODE"]

    literalNames = [
        "<INVALID>",
        "'fun'",
        "'('",
        "')'",
        "'{'",
        "'}'",
        "','",
        "'='",
        "'print('",
        "'execute('",
        "'return'",
        "'set_actor_name('",
        "':'",
        "'ActorFactory.create('",
        "'SEQUENCE'",
        "'ITE'",
        "'REPEAT'",
        "'PARALLEL'",
        "'PARALLEL_PORTFOLIO'",
        "'Joiner'",
        "'Setter'",
        "'Comparator'",
        "'Copy'",
        "'Rename'",
        "'TestSpecToSpec()'",
        "'SpecToTestSpec()'",
        "'ClassificationToActorDefinition()'",
        "'Identity'",
        "'ArtifactFactory.create('",
        "'.'",
        "'ILP32'",
        "'LP64'",
        "'NOT'",
        "'INSTANCEOF'",
        "'ELEMENTOF'",
        "'TODO---'",
        "'''",
        "';'",
    ]

    symbolicNames = [
        "<INVALID>",
        "VERDICT",
        "BIN_OP",
        "ID",
        "STRING",
        "TYPE_NAME",
        "LETTER",
        "LOWER_CASE",
        "UPPER_CASE",
        "DIGIT",
        "NEWLINE",
        "WS",
        "DELIMITER",
        "COMMENT",
    ]

    ruleNames = [
        "T__0",
        "T__1",
        "T__2",
        "T__3",
        "T__4",
        "T__5",
        "T__6",
        "T__7",
        "T__8",
        "T__9",
        "T__10",
        "T__11",
        "T__12",
        "T__13",
        "T__14",
        "T__15",
        "T__16",
        "T__17",
        "T__18",
        "T__19",
        "T__20",
        "T__21",
        "T__22",
        "T__23",
        "T__24",
        "T__25",
        "T__26",
        "T__27",
        "T__28",
        "T__29",
        "T__30",
        "T__31",
        "T__32",
        "T__33",
        "T__34",
        "T__35",
        "VERDICT",
        "BIN_OP",
        "ID",
        "STRING",
        "TYPE_NAME",
        "LETTER",
        "LOWER_CASE",
        "UPPER_CASE",
        "DIGIT",
        "NEWLINE",
        "WS",
        "DELIMITER",
        "COMMENT",
    ]

    grammarFileName = "CoVeriLang.g4"

    def __init__(self, input=None, output: TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = LexerATNSimulator(
            self, self.atn, self.decisionsToDFA, PredictionContextCache()
        )
        self._actions = None
        self._predicates = None

    def action(self, localctx: RuleContext, ruleIndex: int, actionIndex: int):
        if self._actions is None:
            actions = dict()
            actions[36] = self.VERDICT_action
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))

    def VERDICT_action(self, localctx: RuleContext, actionIndex: int):
        if actionIndex == 0:
            self.text = "RESULT_CLASS_FALSE"

        if actionIndex == 1:
            self.text = "RESULT_CLASS_TRUE"

        if actionIndex == 2:
            self.text = "RESULT_CLASS_OTHER"
