# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# Generated from CoVeriLang.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys

if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\63")
        buf.write("\u016a\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\3\2\7\2\66\n\2\f\2\16\29\13\2\3\2\3\2")
        buf.write("\3\3\3\3\3\3\3\3\5\3A\n\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4")
        buf.write("\3\4\7\4K\n\4\f\4\16\4N\13\4\3\5\3\5\3\5\7\5S\n\5\f\5")
        buf.write("\16\5V\13\5\3\6\3\6\3\6\3\6\3\6\5\6]\n\6\3\7\3\7\3\7\3")
        buf.write("\7\3\b\3\b\3\b\3\b\3\b\5\bh\n\b\3\b\3\b\3\t\3\t\3\t\3")
        buf.write("\t\3\t\5\tq\n\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3\13\3\13")
        buf.write("\3\13\3\13\3\f\3\f\3\f\3\f\3\r\3\r\3\r\7\r\u0085\n\r\f")
        buf.write("\r\16\r\u0088\13\r\3\16\3\16\3\16\3\16\3\16\5\16\u008f")
        buf.write("\n\16\5\16\u0091\n\16\3\17\3\17\3\17\3\17\3\17\3\17\5")
        buf.write("\17\u0099\n\17\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u00a1")
        buf.write("\n\20\3\20\3\20\3\20\3\20\3\20\5\20\u00a8\n\20\3\20\3")
        buf.write("\20\3\20\3\20\3\20\3\20\3\20\6\20\u00b1\n\20\r\20\16\20")
        buf.write("\u00b2\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\5")
        buf.write("\20\u00be\n\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20")
        buf.write("\3\20\3\20\3\20\3\20\3\20\3\20\6\20\u00ce\n\20\r\20\16")
        buf.write("\20\u00cf\3\20\3\20\3\20\3\20\3\20\3\20\3\20\7\20\u00d9")
        buf.write("\n\20\f\20\16\20\u00dc\13\20\3\20\3\20\3\20\3\20\3\20")
        buf.write("\3\20\3\20\3\20\3\20\3\20\5\20\u00e8\n\20\3\21\3\21\3")
        buf.write("\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21")
        buf.write("\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21")
        buf.write("\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21")
        buf.write("\3\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u0114\n\21\3")
        buf.write("\21\3\21\5\21\u0118\n\21\3\22\3\22\3\22\3\22\3\22\3\22")
        buf.write("\5\22\u0120\n\22\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u0128")
        buf.write("\n\22\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26")
        buf.write("\u0149\n\26\3\26\3\26\3\26\7\26\u014e\n\26\f\26\16\26")
        buf.write("\u0151\13\26\3\27\3\27\3\27\7\27\u0156\n\27\f\27\16\27")
        buf.write("\u0159\13\27\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\3")
        buf.write("\31\5\31\u0164\n\31\3\32\3\32\3\32\3\32\3\32\3\67\3*\33")
        buf.write('\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 "$&(*,.\60\62')
        buf.write("\2\4\3\2)*\4\2 !))\2\u0188\2\67\3\2\2\2\4<\3\2\2\2\6L")
        buf.write("\3\2\2\2\bO\3\2\2\2\n\\\3\2\2\2\f^\3\2\2\2\16b\3\2\2\2")
        buf.write("\20k\3\2\2\2\22t\3\2\2\2\24w\3\2\2\2\26}\3\2\2\2\30\u0081")
        buf.write("\3\2\2\2\32\u0089\3\2\2\2\34\u0098\3\2\2\2\36\u00e7\3")
        buf.write('\2\2\2 \u0117\3\2\2\2"\u0127\3\2\2\2$\u0129\3\2\2\2&')
        buf.write("\u012b\3\2\2\2(\u012d\3\2\2\2*\u0148\3\2\2\2,\u0152\3")
        buf.write("\2\2\2.\u015a\3\2\2\2\60\u0163\3\2\2\2\62\u0165\3\2\2")
        buf.write("\2\64\66\5\4\3\2\65\64\3\2\2\2\669\3\2\2\2\678\3\2\2\2")
        buf.write("\67\65\3\2\2\28:\3\2\2\29\67\3\2\2\2:;\5\6\4\2;\3\3\2")
        buf.write("\2\2<=\7\3\2\2=>\7)\2\2>@\7\4\2\2?A\5\b\5\2@?\3\2\2\2")
        buf.write("@A\3\2\2\2AB\3\2\2\2BC\7\5\2\2CD\7\6\2\2DE\5\6\4\2EF\7")
        buf.write("\7\2\2F\5\3\2\2\2GH\5\n\6\2HI\7\62\2\2IK\3\2\2\2JG\3\2")
        buf.write("\2\2KN\3\2\2\2LJ\3\2\2\2LM\3\2\2\2M\7\3\2\2\2NL\3\2\2")
        buf.write("\2OT\7)\2\2PQ\7\b\2\2QS\7)\2\2RP\3\2\2\2SV\3\2\2\2TR\3")
        buf.write("\2\2\2TU\3\2\2\2U\t\3\2\2\2VT\3\2\2\2W]\5\f\7\2X]\5\20")
        buf.write("\t\2Y]\5\22\n\2Z]\5\16\b\2[]\5\24\13\2\\W\3\2\2\2\\X\3")
        buf.write("\2\2\2\\Y\3\2\2\2\\Z\3\2\2\2\\[\3\2\2\2]\13\3\2\2\2^_")
        buf.write("\7)\2\2_`\7\t\2\2`a\5\34\17\2a\r\3\2\2\2bg\7\n\2\2ch\5")
        buf.write('\36\20\2dh\5"\22\2eh\7*\2\2fh\7)\2\2gc\3\2\2\2gd\3\2')
        buf.write("\2\2ge\3\2\2\2gf\3\2\2\2hi\3\2\2\2ij\7\5\2\2j\17\3\2\2")
        buf.write("\2kl\7\13\2\2lm\5\36\20\2mp\7\b\2\2nq\7)\2\2oq\5\26\f")
        buf.write("\2pn\3\2\2\2po\3\2\2\2qr\3\2\2\2rs\7\5\2\2s\21\3\2\2\2")
        buf.write("tu\7\f\2\2uv\7)\2\2v\23\3\2\2\2wx\7\r\2\2xy\7)\2\2yz\7")
        buf.write("\b\2\2z{\7*\2\2{|\7\5\2\2|\25\3\2\2\2}~\7\6\2\2~\177\5")
        buf.write("\30\r\2\177\u0080\7\7\2\2\u0080\27\3\2\2\2\u0081\u0086")
        buf.write("\5\32\16\2\u0082\u0083\7\b\2\2\u0083\u0085\5\32\16\2\u0084")
        buf.write("\u0082\3\2\2\2\u0085\u0088\3\2\2\2\u0086\u0084\3\2\2\2")
        buf.write("\u0086\u0087\3\2\2\2\u0087\31\3\2\2\2\u0088\u0086\3\2")
        buf.write("\2\2\u0089\u0090\5\60\31\2\u008a\u008e\7\16\2\2\u008b")
        buf.write('\u008f\5"\22\2\u008c\u008f\5$\23\2\u008d\u008f\5\62\32')
        buf.write("\2\u008e\u008b\3\2\2\2\u008e\u008c\3\2\2\2\u008e\u008d")
        buf.write("\3\2\2\2\u008f\u0091\3\2\2\2\u0090\u008a\3\2\2\2\u0090")
        buf.write("\u0091\3\2\2\2\u0091\33\3\2\2\2\u0092\u0099\5*\26\2\u0093")
        buf.write('\u0099\5"\22\2\u0094\u0099\5\26\f\2\u0095\u0099\5\36')
        buf.write("\20\2\u0096\u0099\7*\2\2\u0097\u0099\5\20\t\2\u0098\u0092")
        buf.write("\3\2\2\2\u0098\u0093\3\2\2\2\u0098\u0094\3\2\2\2\u0098")
        buf.write("\u0095\3\2\2\2\u0098\u0096\3\2\2\2\u0098\u0097\3\2\2\2")
        buf.write("\u0099\35\3\2\2\2\u009a\u009b\7\17\2\2\u009b\u009c\5(")
        buf.write("\25\2\u009c\u009d\7\b\2\2\u009d\u00a0\t\2\2\2\u009e\u009f")
        buf.write("\7\b\2\2\u009f\u00a1\t\2\2\2\u00a0\u009e\3\2\2\2\u00a0")
        buf.write("\u00a1\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00a3\7\5\2\2")
        buf.write("\u00a3\u00e8\3\2\2\2\u00a4\u00a5\7)\2\2\u00a5\u00a7\7")
        buf.write("\4\2\2\u00a6\u00a8\5\b\5\2\u00a7\u00a6\3\2\2\2\u00a7\u00a8")
        buf.write("\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9\u00e8\7\5\2\2\u00aa")
        buf.write("\u00e8\5 \21\2\u00ab\u00ac\7\20\2\2\u00ac\u00ad\7\4\2")
        buf.write("\2\u00ad\u00b0\5\36\20\2\u00ae\u00af\7\b\2\2\u00af\u00b1")
        buf.write("\5\36\20\2\u00b0\u00ae\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2")
        buf.write("\u00b0\3\2\2\2\u00b2\u00b3\3\2\2\2\u00b3\u00b4\3\2\2\2")
        buf.write("\u00b4\u00b5\7\5\2\2\u00b5\u00e8\3\2\2\2\u00b6\u00b7\7")
        buf.write("\21\2\2\u00b7\u00b8\7\4\2\2\u00b8\u00b9\5*\26\2\u00b9")
        buf.write("\u00ba\7\b\2\2\u00ba\u00bd\5\36\20\2\u00bb\u00bc\7\b\2")
        buf.write("\2\u00bc\u00be\5\36\20\2\u00bd\u00bb\3\2\2\2\u00bd\u00be")
        buf.write("\3\2\2\2\u00be\u00bf\3\2\2\2\u00bf\u00c0\7\5\2\2\u00c0")
        buf.write("\u00e8\3\2\2\2\u00c1\u00c2\7\22\2\2\u00c2\u00c3\7\4\2")
        buf.write("\2\u00c3\u00c4\5*\26\2\u00c4\u00c5\7\b\2\2\u00c5\u00c6")
        buf.write("\5\36\20\2\u00c6\u00c7\7\5\2\2\u00c7\u00e8\3\2\2\2\u00c8")
        buf.write("\u00c9\7\23\2\2\u00c9\u00ca\7\4\2\2\u00ca\u00cd\5\36\20")
        buf.write("\2\u00cb\u00cc\7\b\2\2\u00cc\u00ce\5\36\20\2\u00cd\u00cb")
        buf.write("\3\2\2\2\u00ce\u00cf\3\2\2\2\u00cf\u00cd\3\2\2\2\u00cf")
        buf.write("\u00d0\3\2\2\2\u00d0\u00d1\3\2\2\2\u00d1\u00d2\7\5\2\2")
        buf.write("\u00d2\u00e8\3\2\2\2\u00d3\u00d4\7\24\2\2\u00d4\u00d5")
        buf.write("\7\4\2\2\u00d5\u00da\5\36\20\2\u00d6\u00d7\7\b\2\2\u00d7")
        buf.write("\u00d9\5\36\20\2\u00d8\u00d6\3\2\2\2\u00d9\u00dc\3\2\2")
        buf.write("\2\u00da\u00d8\3\2\2\2\u00da\u00db\3\2\2\2\u00db\u00dd")
        buf.write("\3\2\2\2\u00dc\u00da\3\2\2\2\u00dd\u00de\7\b\2\2\u00de")
        buf.write("\u00df\5*\26\2\u00df\u00e0\3\2\2\2\u00e0\u00e1\7\5\2\2")
        buf.write("\u00e1\u00e8\3\2\2\2\u00e2\u00e8\7)\2\2\u00e3\u00e4\7")
        buf.write("\4\2\2\u00e4\u00e5\5\36\20\2\u00e5\u00e6\7\5\2\2\u00e6")
        buf.write("\u00e8\3\2\2\2\u00e7\u009a\3\2\2\2\u00e7\u00a4\3\2\2\2")
        buf.write("\u00e7\u00aa\3\2\2\2\u00e7\u00ab\3\2\2\2\u00e7\u00b6\3")
        buf.write("\2\2\2\u00e7\u00c1\3\2\2\2\u00e7\u00c8\3\2\2\2\u00e7\u00d3")
        buf.write("\3\2\2\2\u00e7\u00e2\3\2\2\2\u00e7\u00e3\3\2\2\2\u00e8")
        buf.write("\37\3\2\2\2\u00e9\u00ea\7\25\2\2\u00ea\u00eb\7\4\2\2\u00eb")
        buf.write("\u00ec\5$\23\2\u00ec\u00ed\7\b\2\2\u00ed\u00ee\5\26\f")
        buf.write("\2\u00ee\u00ef\7\b\2\2\u00ef\u00f0\5\62\32\2\u00f0\u00f1")
        buf.write("\7\5\2\2\u00f1\u0118\3\2\2\2\u00f2\u00f3\7\26\2\2\u00f3")
        buf.write("\u00f4\7\4\2\2\u00f4\u00f5\5\62\32\2\u00f5\u00f6\7\b\2")
        buf.write("\2\u00f6\u00f7\7)\2\2\u00f7\u00f8\7\5\2\2\u00f8\u0118")
        buf.write("\3\2\2\2\u00f9\u00fa\7\27\2\2\u00fa\u00fb\7\4\2\2\u00fb")
        buf.write("\u00fc\5$\23\2\u00fc\u00fd\7\b\2\2\u00fd\u00fe\5\26\f")
        buf.write("\2\u00fe\u00ff\7\b\2\2\u00ff\u0100\5\62\32\2\u0100\u0101")
        buf.write("\7\5\2\2\u0101\u0118\3\2\2\2\u0102\u0103\7\30\2\2\u0103")
        buf.write("\u0104\7\4\2\2\u0104\u0105\5\26\f\2\u0105\u0106\7\5\2")
        buf.write("\2\u0106\u0118\3\2\2\2\u0107\u0108\7\31\2\2\u0108\u0109")
        buf.write("\7\4\2\2\u0109\u010a\5\26\f\2\u010a\u010b\7\5\2\2\u010b")
        buf.write("\u0118\3\2\2\2\u010c\u0118\7\32\2\2\u010d\u0118\7\33\2")
        buf.write("\2\u010e\u0118\7\34\2\2\u010f\u0110\7\35\2\2\u0110\u0113")
        buf.write("\7\4\2\2\u0111\u0114\5\36\20\2\u0112\u0114\5\26\f\2\u0113")
        buf.write("\u0111\3\2\2\2\u0113\u0112\3\2\2\2\u0114\u0115\3\2\2\2")
        buf.write("\u0115\u0116\7\5\2\2\u0116\u0118\3\2\2\2\u0117\u00e9\3")
        buf.write("\2\2\2\u0117\u00f2\3\2\2\2\u0117\u00f9\3\2\2\2\u0117\u0102")
        buf.write("\3\2\2\2\u0117\u0107\3\2\2\2\u0117\u010c\3\2\2\2\u0117")
        buf.write("\u010d\3\2\2\2\u0117\u010e\3\2\2\2\u0117\u010f\3\2\2\2")
        buf.write("\u0118!\3\2\2\2\u0119\u011a\7\36\2\2\u011a\u011b\5$\23")
        buf.write("\2\u011b\u011c\7\b\2\2\u011c\u011f\t\2\2\2\u011d\u011e")
        buf.write("\7\b\2\2\u011e\u0120\5&\24\2\u011f\u011d\3\2\2\2\u011f")
        buf.write("\u0120\3\2\2\2\u0120\u0121\3\2\2\2\u0121\u0122\7\5\2\2")
        buf.write("\u0122\u0128\3\2\2\2\u0123\u0128\7)\2\2\u0124\u0125\7")
        buf.write(")\2\2\u0125\u0126\7\37\2\2\u0126\u0128\7)\2\2\u0127\u0119")
        buf.write("\3\2\2\2\u0127\u0123\3\2\2\2\u0127\u0124\3\2\2\2\u0128")
        buf.write("#\3\2\2\2\u0129\u012a\7+\2\2\u012a%\3\2\2\2\u012b\u012c")
        buf.write("\t\3\2\2\u012c'\3\2\2\2\u012d\u012e\7+\2\2\u012e)\3\2")
        buf.write('\2\2\u012f\u0130\b\26\1\2\u0130\u0131\7"\2\2\u0131\u0149')
        buf.write("\5*\26\b\u0132\u0133\7#\2\2\u0133\u0134\7\4\2\2\u0134")
        buf.write("\u0135\7)\2\2\u0135\u0136\7\b\2\2\u0136\u0137\5$\23\2")
        buf.write("\u0137\u0138\7\5\2\2\u0138\u0149\3\2\2\2\u0139\u013a\7")
        buf.write("$\2\2\u013a\u013b\7\4\2\2\u013b\u013c\7)\2\2\u013c\u013d")
        buf.write("\7\b\2\2\u013d\u013e\7\6\2\2\u013e\u013f\5,\27\2\u013f")
        buf.write("\u0140\7\7\2\2\u0140\u0141\7\5\2\2\u0141\u0149\3\2\2\2")
        buf.write("\u0142\u0149\7)\2\2\u0143\u0149\5\62\32\2\u0144\u0145")
        buf.write("\7\4\2\2\u0145\u0146\5*\26\2\u0146\u0147\7\5\2\2\u0147")
        buf.write("\u0149\3\2\2\2\u0148\u012f\3\2\2\2\u0148\u0132\3\2\2\2")
        buf.write("\u0148\u0139\3\2\2\2\u0148\u0142\3\2\2\2\u0148\u0143\3")
        buf.write("\2\2\2\u0148\u0144\3\2\2\2\u0149\u014f\3\2\2\2\u014a\u014b")
        buf.write("\f\t\2\2\u014b\u014c\7(\2\2\u014c\u014e\5*\26\n\u014d")
        buf.write("\u014a\3\2\2\2\u014e\u0151\3\2\2\2\u014f\u014d\3\2\2\2")
        buf.write("\u014f\u0150\3\2\2\2\u0150+\3\2\2\2\u0151\u014f\3\2\2")
        buf.write("\2\u0152\u0157\7'\2\2\u0153\u0154\7\b\2\2\u0154\u0156")
        buf.write("\7'\2\2\u0155\u0153\3\2\2\2\u0156\u0159\3\2\2\2\u0157")
        buf.write("\u0155\3\2\2\2\u0157\u0158\3\2\2\2\u0158-\3\2\2\2\u0159")
        buf.write("\u0157\3\2\2\2\u015a\u015b\7%\2\2\u015b/\3\2\2\2\u015c")
        buf.write("\u0164\5\62\32\2\u015d\u015e\7\4\2\2\u015e\u015f\5\62")
        buf.write("\32\2\u015f\u0160\7\b\2\2\u0160\u0161\7+\2\2\u0161\u0162")
        buf.write("\7\5\2\2\u0162\u0164\3\2\2\2\u0163\u015c\3\2\2\2\u0163")
        buf.write("\u015d\3\2\2\2\u0164\61\3\2\2\2\u0165\u0166\7&\2\2\u0166")
        buf.write("\u0167\7)\2\2\u0167\u0168\7&\2\2\u0168\63\3\2\2\2\34\67")
        buf.write("@LT\\gp\u0086\u008e\u0090\u0098\u00a0\u00a7\u00b2\u00bd")
        buf.write("\u00cf\u00da\u00e7\u0113\u0117\u011f\u0127\u0148\u014f")
        buf.write("\u0157\u0163")
        return buf.getvalue()


class CoVeriLangParser(Parser):

    grammarFileName = "CoVeriLang.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [DFA(ds, i) for i, ds in enumerate(atn.decisionToState)]

    sharedContextCache = PredictionContextCache()

    literalNames = [
        "<INVALID>",
        "'fun'",
        "'('",
        "')'",
        "'{'",
        "'}'",
        "','",
        "'='",
        "'print('",
        "'execute('",
        "'return'",
        "'set_actor_name('",
        "':'",
        "'ActorFactory.create('",
        "'SEQUENCE'",
        "'ITE'",
        "'REPEAT'",
        "'PARALLEL'",
        "'PARALLEL_PORTFOLIO'",
        "'Joiner'",
        "'Setter'",
        "'Comparator'",
        "'Copy'",
        "'Rename'",
        "'TestSpecToSpec()'",
        "'SpecToTestSpec()'",
        "'ClassificationToActorDefinition()'",
        "'Identity'",
        "'ArtifactFactory.create('",
        "'.'",
        "'ILP32'",
        "'LP64'",
        "'NOT'",
        "'INSTANCEOF'",
        "'ELEMENTOF'",
        "'TODO---'",
        "'''",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "';'",
    ]

    symbolicNames = [
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "<INVALID>",
        "VERDICT",
        "BIN_OP",
        "ID",
        "STRING",
        "TYPE_NAME",
        "LETTER",
        "LOWER_CASE",
        "UPPER_CASE",
        "DIGIT",
        "NEWLINE",
        "WS",
        "DELIMITER",
        "COMMENT",
    ]

    RULE_program = 0
    RULE_fun_decl = 1
    RULE_stmt_block = 2
    RULE_id_list = 3
    RULE_stmt = 4
    RULE_spec_stmt = 5
    RULE_print_stmt = 6
    RULE_exec_stmt = 7
    RULE_return_stmt = 8
    RULE_set_actor_name_stmt = 9
    RULE_arg_map = 10
    RULE_map_item_list = 11
    RULE_map_item = 12
    RULE_assignable = 13
    RULE_actor = 14
    RULE_utility_actor = 15
    RULE_artifact = 16
    RULE_artifact_type = 17
    RULE_data_model = 18
    RULE_actor_type = 19
    RULE_exp = 20
    RULE_verdict_list = 21
    RULE_tc_exp = 22
    RULE_quoted_ID_with_maybe_type = 23
    RULE_quoted_ID = 24

    ruleNames = [
        "program",
        "fun_decl",
        "stmt_block",
        "id_list",
        "stmt",
        "spec_stmt",
        "print_stmt",
        "exec_stmt",
        "return_stmt",
        "set_actor_name_stmt",
        "arg_map",
        "map_item_list",
        "map_item",
        "assignable",
        "actor",
        "utility_actor",
        "artifact",
        "artifact_type",
        "data_model",
        "actor_type",
        "exp",
        "verdict_list",
        "tc_exp",
        "quoted_ID_with_maybe_type",
        "quoted_ID",
    ]

    EOF = Token.EOF
    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    T__15 = 16
    T__16 = 17
    T__17 = 18
    T__18 = 19
    T__19 = 20
    T__20 = 21
    T__21 = 22
    T__22 = 23
    T__23 = 24
    T__24 = 25
    T__25 = 26
    T__26 = 27
    T__27 = 28
    T__28 = 29
    T__29 = 30
    T__30 = 31
    T__31 = 32
    T__32 = 33
    T__33 = 34
    T__34 = 35
    T__35 = 36
    VERDICT = 37
    BIN_OP = 38
    ID = 39
    STRING = 40
    TYPE_NAME = 41
    LETTER = 42
    LOWER_CASE = 43
    UPPER_CASE = 44
    DIGIT = 45
    NEWLINE = 46
    WS = 47
    DELIMITER = 48
    COMMENT = 49

    def __init__(self, input: TokenStream, output: TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(
            self, self.atn, self.decisionsToDFA, self.sharedContextCache
        )
        self._predicates = None

    class ProgramContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def stmt_block(self):
            return self.getTypedRuleContext(CoVeriLangParser.Stmt_blockContext, 0)

        def fun_decl(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.Fun_declContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.Fun_declContext, i)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_program

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterProgram"):
                listener.enterProgram(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitProgram"):
                listener.exitProgram(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitProgram"):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)

    def program(self):

        localctx = CoVeriLangParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 53
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input, 0, self._ctx)
            while _alt != 1 and _alt != ATN.INVALID_ALT_NUMBER:
                if _alt == 1 + 1:
                    self.state = 50
                    self.fun_decl()
                self.state = 55
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input, 0, self._ctx)

            self.state = 56
            self.stmt_block()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Fun_declContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def stmt_block(self):
            return self.getTypedRuleContext(CoVeriLangParser.Stmt_blockContext, 0)

        def id_list(self):
            return self.getTypedRuleContext(CoVeriLangParser.Id_listContext, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_fun_decl

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterFun_decl"):
                listener.enterFun_decl(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitFun_decl"):
                listener.exitFun_decl(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitFun_decl"):
                return visitor.visitFun_decl(self)
            else:
                return visitor.visitChildren(self)

    def fun_decl(self):

        localctx = CoVeriLangParser.Fun_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_fun_decl)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 58
            self.match(CoVeriLangParser.T__0)
            self.state = 59
            self.match(CoVeriLangParser.ID)
            self.state = 60
            self.match(CoVeriLangParser.T__1)
            self.state = 62
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la == CoVeriLangParser.ID:
                self.state = 61
                self.id_list()

            self.state = 64
            self.match(CoVeriLangParser.T__2)
            self.state = 65
            self.match(CoVeriLangParser.T__3)
            self.state = 66
            self.stmt_block()
            self.state = 67
            self.match(CoVeriLangParser.T__4)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Stmt_blockContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def stmt(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.StmtContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.StmtContext, i)

        def DELIMITER(self, i: int = None):
            if i is None:
                return self.getTokens(CoVeriLangParser.DELIMITER)
            else:
                return self.getToken(CoVeriLangParser.DELIMITER, i)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_stmt_block

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterStmt_block"):
                listener.enterStmt_block(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitStmt_block"):
                listener.exitStmt_block(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitStmt_block"):
                return visitor.visitStmt_block(self)
            else:
                return visitor.visitChildren(self)

    def stmt_block(self):

        localctx = CoVeriLangParser.Stmt_blockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_stmt_block)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 74
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((_la) & ~0x3F) == 0 and (
                (1 << _la)
                & (
                    (1 << CoVeriLangParser.T__7)
                    | (1 << CoVeriLangParser.T__8)
                    | (1 << CoVeriLangParser.T__9)
                    | (1 << CoVeriLangParser.T__10)
                    | (1 << CoVeriLangParser.ID)
                )
            ) != 0:
                self.state = 69
                self.stmt()
                self.state = 70
                self.match(CoVeriLangParser.DELIMITER)
                self.state = 76
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Id_listContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i: int = None):
            if i is None:
                return self.getTokens(CoVeriLangParser.ID)
            else:
                return self.getToken(CoVeriLangParser.ID, i)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_id_list

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterId_list"):
                listener.enterId_list(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitId_list"):
                listener.exitId_list(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitId_list"):
                return visitor.visitId_list(self)
            else:
                return visitor.visitChildren(self)

    def id_list(self):

        localctx = CoVeriLangParser.Id_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_id_list)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 77
            self.match(CoVeriLangParser.ID)
            self.state = 82
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == CoVeriLangParser.T__5:
                self.state = 78
                self.match(CoVeriLangParser.T__5)
                self.state = 79
                self.match(CoVeriLangParser.ID)
                self.state = 84
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StmtContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def spec_stmt(self):
            return self.getTypedRuleContext(CoVeriLangParser.Spec_stmtContext, 0)

        def exec_stmt(self):
            return self.getTypedRuleContext(CoVeriLangParser.Exec_stmtContext, 0)

        def return_stmt(self):
            return self.getTypedRuleContext(CoVeriLangParser.Return_stmtContext, 0)

        def print_stmt(self):
            return self.getTypedRuleContext(CoVeriLangParser.Print_stmtContext, 0)

        def set_actor_name_stmt(self):
            return self.getTypedRuleContext(
                CoVeriLangParser.Set_actor_name_stmtContext, 0
            )

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_stmt

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterStmt"):
                listener.enterStmt(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitStmt"):
                listener.exitStmt(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitStmt"):
                return visitor.visitStmt(self)
            else:
                return visitor.visitChildren(self)

    def stmt(self):

        localctx = CoVeriLangParser.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_stmt)
        try:
            self.state = 90
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CoVeriLangParser.ID]:
                self.enterOuterAlt(localctx, 1)
                self.state = 85
                self.spec_stmt()
                pass
            elif token in [CoVeriLangParser.T__8]:
                self.enterOuterAlt(localctx, 2)
                self.state = 86
                self.exec_stmt()
                pass
            elif token in [CoVeriLangParser.T__9]:
                self.enterOuterAlt(localctx, 3)
                self.state = 87
                self.return_stmt()
                pass
            elif token in [CoVeriLangParser.T__7]:
                self.enterOuterAlt(localctx, 4)
                self.state = 88
                self.print_stmt()
                pass
            elif token in [CoVeriLangParser.T__10]:
                self.enterOuterAlt(localctx, 5)
                self.state = 89
                self.set_actor_name_stmt()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Spec_stmtContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def assignable(self):
            return self.getTypedRuleContext(CoVeriLangParser.AssignableContext, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_spec_stmt

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterSpec_stmt"):
                listener.enterSpec_stmt(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitSpec_stmt"):
                listener.exitSpec_stmt(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitSpec_stmt"):
                return visitor.visitSpec_stmt(self)
            else:
                return visitor.visitChildren(self)

    def spec_stmt(self):

        localctx = CoVeriLangParser.Spec_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_spec_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 92
            self.match(CoVeriLangParser.ID)
            self.state = 93
            self.match(CoVeriLangParser.T__6)
            self.state = 94
            self.assignable()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Print_stmtContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_print_stmt

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class PrintActorContext(Print_stmtContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Print_stmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.ActorContext, 0)

        def artifact(self):
            return self.getTypedRuleContext(CoVeriLangParser.ArtifactContext, 0)

        def STRING(self):
            return self.getToken(CoVeriLangParser.STRING, 0)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterPrintActor"):
                listener.enterPrintActor(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitPrintActor"):
                listener.exitPrintActor(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitPrintActor"):
                return visitor.visitPrintActor(self)
            else:
                return visitor.visitChildren(self)

    def print_stmt(self):

        localctx = CoVeriLangParser.Print_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_print_stmt)
        try:
            localctx = CoVeriLangParser.PrintActorContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 96
            self.match(CoVeriLangParser.T__7)
            self.state = 101
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input, 5, self._ctx)
            if la_ == 1:
                self.state = 97
                self.actor()
                pass

            elif la_ == 2:
                self.state = 98
                self.artifact()
                pass

            elif la_ == 3:
                self.state = 99
                self.match(CoVeriLangParser.STRING)
                pass

            elif la_ == 4:
                self.state = 100
                self.match(CoVeriLangParser.ID)
                pass

            self.state = 103
            self.match(CoVeriLangParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Exec_stmtContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_exec_stmt

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class ExecuteActorContext(Exec_stmtContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Exec_stmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.ActorContext, 0)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterExecuteActor"):
                listener.enterExecuteActor(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitExecuteActor"):
                listener.exitExecuteActor(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitExecuteActor"):
                return visitor.visitExecuteActor(self)
            else:
                return visitor.visitChildren(self)

    def exec_stmt(self):

        localctx = CoVeriLangParser.Exec_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_exec_stmt)
        try:
            localctx = CoVeriLangParser.ExecuteActorContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 105
            self.match(CoVeriLangParser.T__8)
            self.state = 106
            self.actor()
            self.state = 107
            self.match(CoVeriLangParser.T__5)
            self.state = 110
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CoVeriLangParser.ID]:
                self.state = 108
                self.match(CoVeriLangParser.ID)
                pass
            elif token in [CoVeriLangParser.T__3]:
                self.state = 109
                self.arg_map()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 112
            self.match(CoVeriLangParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Return_stmtContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_return_stmt

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterReturn_stmt"):
                listener.enterReturn_stmt(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitReturn_stmt"):
                listener.exitReturn_stmt(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitReturn_stmt"):
                return visitor.visitReturn_stmt(self)
            else:
                return visitor.visitChildren(self)

    def return_stmt(self):

        localctx = CoVeriLangParser.Return_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_return_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 114
            self.match(CoVeriLangParser.T__9)
            self.state = 115
            self.match(CoVeriLangParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Set_actor_name_stmtContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_set_actor_name_stmt

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class SetActorNameContext(Set_actor_name_stmtContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Set_actor_name_stmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def STRING(self):
            return self.getToken(CoVeriLangParser.STRING, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterSetActorName"):
                listener.enterSetActorName(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitSetActorName"):
                listener.exitSetActorName(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitSetActorName"):
                return visitor.visitSetActorName(self)
            else:
                return visitor.visitChildren(self)

    def set_actor_name_stmt(self):

        localctx = CoVeriLangParser.Set_actor_name_stmtContext(
            self, self._ctx, self.state
        )
        self.enterRule(localctx, 18, self.RULE_set_actor_name_stmt)
        try:
            localctx = CoVeriLangParser.SetActorNameContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 117
            self.match(CoVeriLangParser.T__10)
            self.state = 118
            self.match(CoVeriLangParser.ID)
            self.state = 119
            self.match(CoVeriLangParser.T__5)
            self.state = 120
            self.match(CoVeriLangParser.STRING)
            self.state = 121
            self.match(CoVeriLangParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Arg_mapContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def map_item_list(self):
            return self.getTypedRuleContext(CoVeriLangParser.Map_item_listContext, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_arg_map

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterArg_map"):
                listener.enterArg_map(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitArg_map"):
                listener.exitArg_map(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitArg_map"):
                return visitor.visitArg_map(self)
            else:
                return visitor.visitChildren(self)

    def arg_map(self):

        localctx = CoVeriLangParser.Arg_mapContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_arg_map)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 123
            self.match(CoVeriLangParser.T__3)
            self.state = 124
            self.map_item_list()
            self.state = 125
            self.match(CoVeriLangParser.T__4)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Map_item_listContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def map_item(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.Map_itemContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.Map_itemContext, i)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_map_item_list

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterMap_item_list"):
                listener.enterMap_item_list(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitMap_item_list"):
                listener.exitMap_item_list(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitMap_item_list"):
                return visitor.visitMap_item_list(self)
            else:
                return visitor.visitChildren(self)

    def map_item_list(self):

        localctx = CoVeriLangParser.Map_item_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_map_item_list)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 127
            self.map_item()
            self.state = 132
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == CoVeriLangParser.T__5:
                self.state = 128
                self.match(CoVeriLangParser.T__5)
                self.state = 129
                self.map_item()
                self.state = 134
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Map_itemContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def quoted_ID_with_maybe_type(self):
            return self.getTypedRuleContext(
                CoVeriLangParser.Quoted_ID_with_maybe_typeContext, 0
            )

        def artifact(self):
            return self.getTypedRuleContext(CoVeriLangParser.ArtifactContext, 0)

        def artifact_type(self):
            return self.getTypedRuleContext(CoVeriLangParser.Artifact_typeContext, 0)

        def quoted_ID(self):
            return self.getTypedRuleContext(CoVeriLangParser.Quoted_IDContext, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_map_item

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterMap_item"):
                listener.enterMap_item(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitMap_item"):
                listener.exitMap_item(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitMap_item"):
                return visitor.visitMap_item(self)
            else:
                return visitor.visitChildren(self)

    def map_item(self):

        localctx = CoVeriLangParser.Map_itemContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_map_item)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 135
            self.quoted_ID_with_maybe_type()
            self.state = 142
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la == CoVeriLangParser.T__11:
                self.state = 136
                self.match(CoVeriLangParser.T__11)
                self.state = 140
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [CoVeriLangParser.T__27, CoVeriLangParser.ID]:
                    self.state = 137
                    self.artifact()
                    pass
                elif token in [CoVeriLangParser.TYPE_NAME]:
                    self.state = 138
                    self.artifact_type()
                    pass
                elif token in [CoVeriLangParser.T__35]:
                    self.state = 139
                    self.quoted_ID()
                    pass
                else:
                    raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AssignableContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp(self):
            return self.getTypedRuleContext(CoVeriLangParser.ExpContext, 0)

        def artifact(self):
            return self.getTypedRuleContext(CoVeriLangParser.ArtifactContext, 0)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.ActorContext, 0)

        def STRING(self):
            return self.getToken(CoVeriLangParser.STRING, 0)

        def exec_stmt(self):
            return self.getTypedRuleContext(CoVeriLangParser.Exec_stmtContext, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_assignable

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterAssignable"):
                listener.enterAssignable(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitAssignable"):
                listener.exitAssignable(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitAssignable"):
                return visitor.visitAssignable(self)
            else:
                return visitor.visitChildren(self)

    def assignable(self):

        localctx = CoVeriLangParser.AssignableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_assignable)
        try:
            self.state = 150
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input, 10, self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 144
                self.exp(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 145
                self.artifact()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 146
                self.arg_map()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 147
                self.actor()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 148
                self.match(CoVeriLangParser.STRING)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 149
                self.exec_stmt()
                pass

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ActorContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_actor

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class UtilityContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def utility_actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.Utility_actorContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterUtility"):
                listener.enterUtility(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitUtility"):
                listener.exitUtility(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitUtility"):
                return visitor.visitUtility(self)
            else:
                return visitor.visitChildren(self)

    class ParenthesisContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.ActorContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterParenthesis"):
                listener.enterParenthesis(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitParenthesis"):
                listener.exitParenthesis(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitParenthesis"):
                return visitor.visitParenthesis(self)
            else:
                return visitor.visitChildren(self)

    class FunCallContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def id_list(self):
            return self.getTypedRuleContext(CoVeriLangParser.Id_listContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterFunCall"):
                listener.enterFunCall(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitFunCall"):
                listener.exitFunCall(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitFunCall"):
                return visitor.visitFunCall(self)
            else:
                return visitor.visitChildren(self)

    class ParallelPortfolioContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.ActorContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.ActorContext, i)

        def exp(self):
            return self.getTypedRuleContext(CoVeriLangParser.ExpContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterParallelPortfolio"):
                listener.enterParallelPortfolio(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitParallelPortfolio"):
                listener.exitParallelPortfolio(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitParallelPortfolio"):
                return visitor.visitParallelPortfolio(self)
            else:
                return visitor.visitChildren(self)

    class AtomicContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.name = None  # Token
            self.version = None  # Token
            self.copyFrom(ctx)

        def actor_type(self):
            return self.getTypedRuleContext(CoVeriLangParser.Actor_typeContext, 0)

        def ID(self, i: int = None):
            if i is None:
                return self.getTokens(CoVeriLangParser.ID)
            else:
                return self.getToken(CoVeriLangParser.ID, i)

        def STRING(self, i: int = None):
            if i is None:
                return self.getTokens(CoVeriLangParser.STRING)
            else:
                return self.getToken(CoVeriLangParser.STRING, i)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterAtomic"):
                listener.enterAtomic(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitAtomic"):
                listener.exitAtomic(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitAtomic"):
                return visitor.visitAtomic(self)
            else:
                return visitor.visitChildren(self)

    class IterativeContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def exp(self):
            return self.getTypedRuleContext(CoVeriLangParser.ExpContext, 0)

        def actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.ActorContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterIterative"):
                listener.enterIterative(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitIterative"):
                listener.exitIterative(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitIterative"):
                return visitor.visitIterative(self)
            else:
                return visitor.visitChildren(self)

    class SequenceContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.ActorContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.ActorContext, i)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterSequence"):
                listener.enterSequence(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitSequence"):
                listener.exitSequence(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitSequence"):
                return visitor.visitSequence(self)
            else:
                return visitor.visitChildren(self)

    class ITEContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def exp(self):
            return self.getTypedRuleContext(CoVeriLangParser.ExpContext, 0)

        def actor(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.ActorContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.ActorContext, i)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterITE"):
                listener.enterITE(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitITE"):
                listener.exitITE(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitITE"):
                return visitor.visitITE(self)
            else:
                return visitor.visitChildren(self)

    class ParallelContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.ActorContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.ActorContext, i)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterParallel"):
                listener.enterParallel(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitParallel"):
                listener.exitParallel(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitParallel"):
                return visitor.visitParallel(self)
            else:
                return visitor.visitChildren(self)

    class ActorAliasContext(ActorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ActorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterActorAlias"):
                listener.enterActorAlias(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitActorAlias"):
                listener.exitActorAlias(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitActorAlias"):
                return visitor.visitActorAlias(self)
            else:
                return visitor.visitChildren(self)

    def actor(self):

        localctx = CoVeriLangParser.ActorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_actor)
        self._la = 0  # Token type
        try:
            self.state = 229
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input, 17, self._ctx)
            if la_ == 1:
                localctx = CoVeriLangParser.AtomicContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 152
                self.match(CoVeriLangParser.T__12)
                self.state = 153
                self.actor_type()
                self.state = 154
                self.match(CoVeriLangParser.T__5)
                self.state = 155
                localctx.name = self._input.LT(1)
                _la = self._input.LA(1)
                if not (_la == CoVeriLangParser.ID or _la == CoVeriLangParser.STRING):
                    localctx.name = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 158
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la == CoVeriLangParser.T__5:
                    self.state = 156
                    self.match(CoVeriLangParser.T__5)
                    self.state = 157
                    localctx.version = self._input.LT(1)
                    _la = self._input.LA(1)
                    if not (
                        _la == CoVeriLangParser.ID or _la == CoVeriLangParser.STRING
                    ):
                        localctx.version = self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()

                self.state = 160
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 2:
                localctx = CoVeriLangParser.FunCallContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 162
                self.match(CoVeriLangParser.ID)
                self.state = 163
                self.match(CoVeriLangParser.T__1)
                self.state = 165
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la == CoVeriLangParser.ID:
                    self.state = 164
                    self.id_list()

                self.state = 167
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 3:
                localctx = CoVeriLangParser.UtilityContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 168
                self.utility_actor()
                pass

            elif la_ == 4:
                localctx = CoVeriLangParser.SequenceContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 169
                self.match(CoVeriLangParser.T__13)
                self.state = 170
                self.match(CoVeriLangParser.T__1)
                self.state = 171
                self.actor()
                self.state = 174
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 172
                    self.match(CoVeriLangParser.T__5)
                    self.state = 173
                    self.actor()
                    self.state = 176
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la == CoVeriLangParser.T__5):
                        break

                self.state = 178
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 5:
                localctx = CoVeriLangParser.ITEContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 180
                self.match(CoVeriLangParser.T__14)
                self.state = 181
                self.match(CoVeriLangParser.T__1)
                self.state = 182
                self.exp(0)
                self.state = 183
                self.match(CoVeriLangParser.T__5)
                self.state = 184
                self.actor()
                self.state = 187
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la == CoVeriLangParser.T__5:
                    self.state = 185
                    self.match(CoVeriLangParser.T__5)
                    self.state = 186
                    self.actor()

                self.state = 189
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 6:
                localctx = CoVeriLangParser.IterativeContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 191
                self.match(CoVeriLangParser.T__15)
                self.state = 192
                self.match(CoVeriLangParser.T__1)
                self.state = 193
                self.exp(0)
                self.state = 194
                self.match(CoVeriLangParser.T__5)
                self.state = 195
                self.actor()
                self.state = 196
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 7:
                localctx = CoVeriLangParser.ParallelContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 198
                self.match(CoVeriLangParser.T__16)
                self.state = 199
                self.match(CoVeriLangParser.T__1)
                self.state = 200
                self.actor()
                self.state = 203
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 201
                    self.match(CoVeriLangParser.T__5)
                    self.state = 202
                    self.actor()
                    self.state = 205
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la == CoVeriLangParser.T__5):
                        break

                self.state = 207
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 8:
                localctx = CoVeriLangParser.ParallelPortfolioContext(self, localctx)
                self.enterOuterAlt(localctx, 8)
                self.state = 209
                self.match(CoVeriLangParser.T__17)
                self.state = 210
                self.match(CoVeriLangParser.T__1)
                self.state = 211
                self.actor()
                self.state = 216
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input, 16, self._ctx)
                while _alt != 2 and _alt != ATN.INVALID_ALT_NUMBER:
                    if _alt == 1:
                        self.state = 212
                        self.match(CoVeriLangParser.T__5)
                        self.state = 213
                        self.actor()
                    self.state = 218
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input, 16, self._ctx)

                self.state = 219
                self.match(CoVeriLangParser.T__5)
                self.state = 220
                self.exp(0)
                self.state = 222
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 9:
                localctx = CoVeriLangParser.ActorAliasContext(self, localctx)
                self.enterOuterAlt(localctx, 9)
                self.state = 224
                self.match(CoVeriLangParser.ID)
                pass

            elif la_ == 10:
                localctx = CoVeriLangParser.ParenthesisContext(self, localctx)
                self.enterOuterAlt(localctx, 10)
                self.state = 225
                self.match(CoVeriLangParser.T__1)
                self.state = 226
                self.actor()
                self.state = 227
                self.match(CoVeriLangParser.T__2)
                pass

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Utility_actorContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_utility_actor

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class JoinerContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def artifact_type(self):
            return self.getTypedRuleContext(CoVeriLangParser.Artifact_typeContext, 0)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def quoted_ID(self):
            return self.getTypedRuleContext(CoVeriLangParser.Quoted_IDContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterJoiner"):
                listener.enterJoiner(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitJoiner"):
                listener.exitJoiner(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitJoiner"):
                return visitor.visitJoiner(self)
            else:
                return visitor.visitChildren(self)

    class TestSpecToSpecContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterTestSpecToSpec"):
                listener.enterTestSpecToSpec(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitTestSpecToSpec"):
                listener.exitTestSpecToSpec(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitTestSpecToSpec"):
                return visitor.visitTestSpecToSpec(self)
            else:
                return visitor.visitChildren(self)

    class SpecToTestSpecContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterSpecToTestSpec"):
                listener.enterSpecToTestSpec(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitSpecToTestSpec"):
                listener.exitSpecToTestSpec(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitSpecToTestSpec"):
                return visitor.visitSpecToTestSpec(self)
            else:
                return visitor.visitChildren(self)

    class CopyContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterCopy"):
                listener.enterCopy(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitCopy"):
                listener.exitCopy(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitCopy"):
                return visitor.visitCopy(self)
            else:
                return visitor.visitChildren(self)

    class SetterContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def quoted_ID(self):
            return self.getTypedRuleContext(CoVeriLangParser.Quoted_IDContext, 0)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterSetter"):
                listener.enterSetter(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitSetter"):
                listener.exitSetter(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitSetter"):
                return visitor.visitSetter(self)
            else:
                return visitor.visitChildren(self)

    class IdentityContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def actor(self):
            return self.getTypedRuleContext(CoVeriLangParser.ActorContext, 0)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterIdentity"):
                listener.enterIdentity(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitIdentity"):
                listener.exitIdentity(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitIdentity"):
                return visitor.visitIdentity(self)
            else:
                return visitor.visitChildren(self)

    class RenameContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterRename"):
                listener.enterRename(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitRename"):
                listener.exitRename(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitRename"):
                return visitor.visitRename(self)
            else:
                return visitor.visitChildren(self)

    class ClassificationToActorDefinitionContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterClassificationToActorDefinition"):
                listener.enterClassificationToActorDefinition(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitClassificationToActorDefinition"):
                listener.exitClassificationToActorDefinition(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitClassificationToActorDefinition"):
                return visitor.visitClassificationToActorDefinition(self)
            else:
                return visitor.visitChildren(self)

    class ComparatorContext(Utility_actorContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.Utility_actorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def artifact_type(self):
            return self.getTypedRuleContext(CoVeriLangParser.Artifact_typeContext, 0)

        def arg_map(self):
            return self.getTypedRuleContext(CoVeriLangParser.Arg_mapContext, 0)

        def quoted_ID(self):
            return self.getTypedRuleContext(CoVeriLangParser.Quoted_IDContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterComparator"):
                listener.enterComparator(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitComparator"):
                listener.exitComparator(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitComparator"):
                return visitor.visitComparator(self)
            else:
                return visitor.visitChildren(self)

    def utility_actor(self):

        localctx = CoVeriLangParser.Utility_actorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_utility_actor)
        try:
            self.state = 277
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CoVeriLangParser.T__18]:
                localctx = CoVeriLangParser.JoinerContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 231
                self.match(CoVeriLangParser.T__18)
                self.state = 232
                self.match(CoVeriLangParser.T__1)
                self.state = 233
                self.artifact_type()
                self.state = 234
                self.match(CoVeriLangParser.T__5)
                self.state = 235
                self.arg_map()
                self.state = 236
                self.match(CoVeriLangParser.T__5)
                self.state = 237
                self.quoted_ID()
                self.state = 238
                self.match(CoVeriLangParser.T__2)
                pass
            elif token in [CoVeriLangParser.T__19]:
                localctx = CoVeriLangParser.SetterContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 240
                self.match(CoVeriLangParser.T__19)
                self.state = 241
                self.match(CoVeriLangParser.T__1)
                self.state = 242
                self.quoted_ID()
                self.state = 243
                self.match(CoVeriLangParser.T__5)
                self.state = 244
                self.match(CoVeriLangParser.ID)
                self.state = 245
                self.match(CoVeriLangParser.T__2)
                pass
            elif token in [CoVeriLangParser.T__20]:
                localctx = CoVeriLangParser.ComparatorContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 247
                self.match(CoVeriLangParser.T__20)
                self.state = 248
                self.match(CoVeriLangParser.T__1)
                self.state = 249
                self.artifact_type()
                self.state = 250
                self.match(CoVeriLangParser.T__5)
                self.state = 251
                self.arg_map()
                self.state = 252
                self.match(CoVeriLangParser.T__5)
                self.state = 253
                self.quoted_ID()
                self.state = 254
                self.match(CoVeriLangParser.T__2)
                pass
            elif token in [CoVeriLangParser.T__21]:
                localctx = CoVeriLangParser.CopyContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 256
                self.match(CoVeriLangParser.T__21)
                self.state = 257
                self.match(CoVeriLangParser.T__1)
                self.state = 258
                self.arg_map()
                self.state = 259
                self.match(CoVeriLangParser.T__2)
                pass
            elif token in [CoVeriLangParser.T__22]:
                localctx = CoVeriLangParser.RenameContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 261
                self.match(CoVeriLangParser.T__22)
                self.state = 262
                self.match(CoVeriLangParser.T__1)
                self.state = 263
                self.arg_map()
                self.state = 264
                self.match(CoVeriLangParser.T__2)
                pass
            elif token in [CoVeriLangParser.T__23]:
                localctx = CoVeriLangParser.TestSpecToSpecContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 266
                self.match(CoVeriLangParser.T__23)
                pass
            elif token in [CoVeriLangParser.T__24]:
                localctx = CoVeriLangParser.SpecToTestSpecContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 267
                self.match(CoVeriLangParser.T__24)
                pass
            elif token in [CoVeriLangParser.T__25]:
                localctx = CoVeriLangParser.ClassificationToActorDefinitionContext(
                    self, localctx
                )
                self.enterOuterAlt(localctx, 8)
                self.state = 268
                self.match(CoVeriLangParser.T__25)
                pass
            elif token in [CoVeriLangParser.T__26]:
                localctx = CoVeriLangParser.IdentityContext(self, localctx)
                self.enterOuterAlt(localctx, 9)
                self.state = 269
                self.match(CoVeriLangParser.T__26)
                self.state = 270
                self.match(CoVeriLangParser.T__1)
                self.state = 273
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [
                    CoVeriLangParser.T__1,
                    CoVeriLangParser.T__12,
                    CoVeriLangParser.T__13,
                    CoVeriLangParser.T__14,
                    CoVeriLangParser.T__15,
                    CoVeriLangParser.T__16,
                    CoVeriLangParser.T__17,
                    CoVeriLangParser.T__18,
                    CoVeriLangParser.T__19,
                    CoVeriLangParser.T__20,
                    CoVeriLangParser.T__21,
                    CoVeriLangParser.T__22,
                    CoVeriLangParser.T__23,
                    CoVeriLangParser.T__24,
                    CoVeriLangParser.T__25,
                    CoVeriLangParser.T__26,
                    CoVeriLangParser.ID,
                ]:
                    self.state = 271
                    self.actor()
                    pass
                elif token in [CoVeriLangParser.T__3]:
                    self.state = 272
                    self.arg_map()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 275
                self.match(CoVeriLangParser.T__2)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ArtifactContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_artifact

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class ArtifactFromMapItemContext(ArtifactContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ArtifactContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self, i: int = None):
            if i is None:
                return self.getTokens(CoVeriLangParser.ID)
            else:
                return self.getToken(CoVeriLangParser.ID, i)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterArtifactFromMapItem"):
                listener.enterArtifactFromMapItem(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitArtifactFromMapItem"):
                listener.exitArtifactFromMapItem(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitArtifactFromMapItem"):
                return visitor.visitArtifactFromMapItem(self)
            else:
                return visitor.visitChildren(self)

    class ArtifactAliasContext(ArtifactContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ArtifactContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterArtifactAlias"):
                listener.enterArtifactAlias(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitArtifactAlias"):
                listener.exitArtifactAlias(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitArtifactAlias"):
                return visitor.visitArtifactAlias(self)
            else:
                return visitor.visitChildren(self)

    class CreateArtifactContext(ArtifactContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ArtifactContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def artifact_type(self):
            return self.getTypedRuleContext(CoVeriLangParser.Artifact_typeContext, 0)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def STRING(self):
            return self.getToken(CoVeriLangParser.STRING, 0)

        def data_model(self):
            return self.getTypedRuleContext(CoVeriLangParser.Data_modelContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterCreateArtifact"):
                listener.enterCreateArtifact(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitCreateArtifact"):
                listener.exitCreateArtifact(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitCreateArtifact"):
                return visitor.visitCreateArtifact(self)
            else:
                return visitor.visitChildren(self)

    def artifact(self):

        localctx = CoVeriLangParser.ArtifactContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_artifact)
        self._la = 0  # Token type
        try:
            self.state = 293
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input, 21, self._ctx)
            if la_ == 1:
                localctx = CoVeriLangParser.CreateArtifactContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 279
                self.match(CoVeriLangParser.T__27)
                self.state = 280
                self.artifact_type()
                self.state = 281
                self.match(CoVeriLangParser.T__5)
                self.state = 282
                _la = self._input.LA(1)
                if not (_la == CoVeriLangParser.ID or _la == CoVeriLangParser.STRING):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 285
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la == CoVeriLangParser.T__5:
                    self.state = 283
                    self.match(CoVeriLangParser.T__5)
                    self.state = 284
                    self.data_model()

                self.state = 287
                self.match(CoVeriLangParser.T__2)
                pass

            elif la_ == 2:
                localctx = CoVeriLangParser.ArtifactAliasContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 289
                self.match(CoVeriLangParser.ID)
                pass

            elif la_ == 3:
                localctx = CoVeriLangParser.ArtifactFromMapItemContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 290
                self.match(CoVeriLangParser.ID)
                self.state = 291
                self.match(CoVeriLangParser.T__28)
                self.state = 292
                self.match(CoVeriLangParser.ID)
                pass

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Artifact_typeContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TYPE_NAME(self):
            return self.getToken(CoVeriLangParser.TYPE_NAME, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_artifact_type

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterArtifact_type"):
                listener.enterArtifact_type(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitArtifact_type"):
                listener.exitArtifact_type(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitArtifact_type"):
                return visitor.visitArtifact_type(self)
            else:
                return visitor.visitChildren(self)

    def artifact_type(self):

        localctx = CoVeriLangParser.Artifact_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_artifact_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 295
            self.match(CoVeriLangParser.TYPE_NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Data_modelContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_data_model

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterData_model"):
                listener.enterData_model(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitData_model"):
                listener.exitData_model(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitData_model"):
                return visitor.visitData_model(self)
            else:
                return visitor.visitChildren(self)

    def data_model(self):

        localctx = CoVeriLangParser.Data_modelContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_data_model)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 297
            _la = self._input.LA(1)
            if not (
                (
                    ((_la) & ~0x3F) == 0
                    and (
                        (1 << _la)
                        & (
                            (1 << CoVeriLangParser.T__29)
                            | (1 << CoVeriLangParser.T__30)
                            | (1 << CoVeriLangParser.ID)
                        )
                    )
                    != 0
                )
            ):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Actor_typeContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TYPE_NAME(self):
            return self.getToken(CoVeriLangParser.TYPE_NAME, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_actor_type

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterActor_type"):
                listener.enterActor_type(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitActor_type"):
                listener.exitActor_type(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitActor_type"):
                return visitor.visitActor_type(self)
            else:
                return visitor.visitChildren(self)

    def actor_type(self):

        localctx = CoVeriLangParser.Actor_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_actor_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 299
            self.match(CoVeriLangParser.TYPE_NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_exp

        def copyFrom(self, ctx: ParserRuleContext):
            super().copyFrom(ctx)

    class ExpAliasContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterExpAlias"):
                listener.enterExpAlias(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitExpAlias"):
                listener.exitExpAlias(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitExpAlias"):
                return visitor.visitExpAlias(self)
            else:
                return visitor.visitChildren(self)

    class NotLogicalContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def exp(self):
            return self.getTypedRuleContext(CoVeriLangParser.ExpContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterNotLogical"):
                listener.enterNotLogical(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitNotLogical"):
                listener.exitNotLogical(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitNotLogical"):
                return visitor.visitNotLogical(self)
            else:
                return visitor.visitChildren(self)

    class InstanceOfContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def artifact_type(self):
            return self.getTypedRuleContext(CoVeriLangParser.Artifact_typeContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterInstanceOf"):
                listener.enterInstanceOf(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitInstanceOf"):
                listener.exitInstanceOf(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitInstanceOf"):
                return visitor.visitInstanceOf(self)
            else:
                return visitor.visitChildren(self)

    class ElementOfContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def verdict_list(self):
            return self.getTypedRuleContext(CoVeriLangParser.Verdict_listContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterElementOf"):
                listener.enterElementOf(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitElementOf"):
                listener.exitElementOf(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitElementOf"):
                return visitor.visitElementOf(self)
            else:
                return visitor.visitChildren(self)

    class QuotedExpAliasForArtifactsContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def quoted_ID(self):
            return self.getTypedRuleContext(CoVeriLangParser.Quoted_IDContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterQuotedExpAliasForArtifacts"):
                listener.enterQuotedExpAliasForArtifacts(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitQuotedExpAliasForArtifacts"):
                listener.exitQuotedExpAliasForArtifacts(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitQuotedExpAliasForArtifacts"):
                return visitor.visitQuotedExpAliasForArtifacts(self)
            else:
                return visitor.visitChildren(self)

    class BinaryLogicalContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def exp(self, i: int = None):
            if i is None:
                return self.getTypedRuleContexts(CoVeriLangParser.ExpContext)
            else:
                return self.getTypedRuleContext(CoVeriLangParser.ExpContext, i)

        def BIN_OP(self):
            return self.getToken(CoVeriLangParser.BIN_OP, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterBinaryLogical"):
                listener.enterBinaryLogical(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitBinaryLogical"):
                listener.exitBinaryLogical(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitBinaryLogical"):
                return visitor.visitBinaryLogical(self)
            else:
                return visitor.visitChildren(self)

    class ParenContext(ExpContext):
        def __init__(
            self, parser, ctx: ParserRuleContext
        ):  # actually a CoVeriLangParser.ExpContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def exp(self):
            return self.getTypedRuleContext(CoVeriLangParser.ExpContext, 0)

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterParen"):
                listener.enterParen(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitParen"):
                listener.exitParen(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitParen"):
                return visitor.visitParen(self)
            else:
                return visitor.visitChildren(self)

    def exp(self, _p: int = 0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = CoVeriLangParser.ExpContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 40
        self.enterRecursionRule(localctx, 40, self.RULE_exp, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 326
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CoVeriLangParser.T__31]:
                localctx = CoVeriLangParser.NotLogicalContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 302
                self.match(CoVeriLangParser.T__31)
                self.state = 303
                self.exp(6)
                pass
            elif token in [CoVeriLangParser.T__32]:
                localctx = CoVeriLangParser.InstanceOfContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 304
                self.match(CoVeriLangParser.T__32)
                self.state = 305
                self.match(CoVeriLangParser.T__1)
                self.state = 306
                self.match(CoVeriLangParser.ID)
                self.state = 307
                self.match(CoVeriLangParser.T__5)
                self.state = 308
                self.artifact_type()
                self.state = 309
                self.match(CoVeriLangParser.T__2)
                pass
            elif token in [CoVeriLangParser.T__33]:
                localctx = CoVeriLangParser.ElementOfContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 311
                self.match(CoVeriLangParser.T__33)
                self.state = 312
                self.match(CoVeriLangParser.T__1)
                self.state = 313
                self.match(CoVeriLangParser.ID)
                self.state = 314
                self.match(CoVeriLangParser.T__5)
                self.state = 315
                self.match(CoVeriLangParser.T__3)
                self.state = 316
                self.verdict_list()
                self.state = 317
                self.match(CoVeriLangParser.T__4)
                self.state = 318
                self.match(CoVeriLangParser.T__2)
                pass
            elif token in [CoVeriLangParser.ID]:
                localctx = CoVeriLangParser.ExpAliasContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 320
                self.match(CoVeriLangParser.ID)
                pass
            elif token in [CoVeriLangParser.T__35]:
                localctx = CoVeriLangParser.QuotedExpAliasForArtifactsContext(
                    self, localctx
                )
                self._ctx = localctx
                _prevctx = localctx
                self.state = 321
                self.quoted_ID()
                pass
            elif token in [CoVeriLangParser.T__1]:
                localctx = CoVeriLangParser.ParenContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 322
                self.match(CoVeriLangParser.T__1)
                self.state = 323
                self.exp(0)
                self.state = 324
                self.match(CoVeriLangParser.T__2)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 333
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input, 23, self._ctx)
            while _alt != 2 and _alt != ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = CoVeriLangParser.BinaryLogicalContext(
                        self,
                        CoVeriLangParser.ExpContext(self, _parentctx, _parentState),
                    )
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp)
                    self.state = 328
                    if not self.precpred(self._ctx, 7):
                        from antlr4.error.Errors import FailedPredicateException

                        raise FailedPredicateException(
                            self, "self.precpred(self._ctx, 7)"
                        )
                    self.state = 329
                    self.match(CoVeriLangParser.BIN_OP)
                    self.state = 330
                    self.exp(8)
                self.state = 335
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input, 23, self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class Verdict_listContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VERDICT(self, i: int = None):
            if i is None:
                return self.getTokens(CoVeriLangParser.VERDICT)
            else:
                return self.getToken(CoVeriLangParser.VERDICT, i)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_verdict_list

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterVerdict_list"):
                listener.enterVerdict_list(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitVerdict_list"):
                listener.exitVerdict_list(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitVerdict_list"):
                return visitor.visitVerdict_list(self)
            else:
                return visitor.visitChildren(self)

    def verdict_list(self):

        localctx = CoVeriLangParser.Verdict_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_verdict_list)
        self._la = 0  # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 336
            self.match(CoVeriLangParser.VERDICT)
            self.state = 341
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la == CoVeriLangParser.T__5:
                self.state = 337
                self.match(CoVeriLangParser.T__5)
                self.state = 338
                self.match(CoVeriLangParser.VERDICT)
                self.state = 343
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Tc_expContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_tc_exp

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterTc_exp"):
                listener.enterTc_exp(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitTc_exp"):
                listener.exitTc_exp(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitTc_exp"):
                return visitor.visitTc_exp(self)
            else:
                return visitor.visitChildren(self)

    def tc_exp(self):

        localctx = CoVeriLangParser.Tc_expContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_tc_exp)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 344
            self.match(CoVeriLangParser.T__34)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Quoted_ID_with_maybe_typeContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def quoted_ID(self):
            return self.getTypedRuleContext(CoVeriLangParser.Quoted_IDContext, 0)

        def TYPE_NAME(self):
            return self.getToken(CoVeriLangParser.TYPE_NAME, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_quoted_ID_with_maybe_type

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterQuoted_ID_with_maybe_type"):
                listener.enterQuoted_ID_with_maybe_type(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitQuoted_ID_with_maybe_type"):
                listener.exitQuoted_ID_with_maybe_type(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitQuoted_ID_with_maybe_type"):
                return visitor.visitQuoted_ID_with_maybe_type(self)
            else:
                return visitor.visitChildren(self)

    def quoted_ID_with_maybe_type(self):

        localctx = CoVeriLangParser.Quoted_ID_with_maybe_typeContext(
            self, self._ctx, self.state
        )
        self.enterRule(localctx, 46, self.RULE_quoted_ID_with_maybe_type)
        try:
            self.state = 353
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CoVeriLangParser.T__35]:
                self.enterOuterAlt(localctx, 1)
                self.state = 346
                self.quoted_ID()
                pass
            elif token in [CoVeriLangParser.T__1]:
                self.enterOuterAlt(localctx, 2)
                self.state = 347
                self.match(CoVeriLangParser.T__1)
                self.state = 348
                self.quoted_ID()
                self.state = 349
                self.match(CoVeriLangParser.T__5)
                self.state = 350
                self.match(CoVeriLangParser.TYPE_NAME)
                self.state = 351
                self.match(CoVeriLangParser.T__2)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Quoted_IDContext(ParserRuleContext):
        def __init__(
            self, parser, parent: ParserRuleContext = None, invokingState: int = -1
        ):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(CoVeriLangParser.ID, 0)

        def getRuleIndex(self):
            return CoVeriLangParser.RULE_quoted_ID

        def enterRule(self, listener: ParseTreeListener):
            if hasattr(listener, "enterQuoted_ID"):
                listener.enterQuoted_ID(self)

        def exitRule(self, listener: ParseTreeListener):
            if hasattr(listener, "exitQuoted_ID"):
                listener.exitQuoted_ID(self)

        def accept(self, visitor: ParseTreeVisitor):
            if hasattr(visitor, "visitQuoted_ID"):
                return visitor.visitQuoted_ID(self)
            else:
                return visitor.visitChildren(self)

    def quoted_ID(self):

        localctx = CoVeriLangParser.Quoted_IDContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_quoted_ID)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 355
            self.match(CoVeriLangParser.T__35)
            self.state = 356
            self.match(CoVeriLangParser.ID)
            self.state = 357
            self.match(CoVeriLangParser.T__35)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    def sempred(self, localctx: RuleContext, ruleIndex: int, predIndex: int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[20] = self.exp_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def exp_sempred(self, localctx: ExpContext, predIndex: int):
        if predIndex == 0:
            return self.precpred(self._ctx, 7)
